require 'time'

def measure(number=1)
 start_time = Time.now
 number.times { yield }
 elapsed_time = Time.now - start_time
 average_time = elapsed_time / number
 average_time
end
